package id.ac.poltek_kediri.informatika.appx10

import android.app.ProgressDialog
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_signup.*

class SignUpActivity : AppCompatActivity(), View.OnClickListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup)
        btnRegister.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        var email = edRegUsername.text.toString()
        var password = edRegPassword.text.toString()

        if(email.isEmpty() || password.isEmpty()){
            Toast.makeText(this, "username / password cant be empty", Toast.LENGTH_LONG).show()
        }
        else{
            val progressDialog = ProgressDialog(this)
            progressDialog.isIndeterminate = true
            progressDialog.setMessage("registering...")
            progressDialog.show()

            FirebaseAuth.getInstance().createUserWithEmailAndPassword(email,password)
                .addOnCompleteListener{
                    progressDialog.hide()
                    if(!it.isSuccessful) return@addOnCompleteListener
                    Toast.makeText(this, "successfully register", Toast.LENGTH_SHORT).show()
                }
                .addOnFailureListener{
                    progressDialog.hide()
                    Toast.makeText(this, "username/ password incorrect", Toast.LENGTH_SHORT).show()
                }
        }
    }
}